/// \file defines.h
/// Essential game engine defines.

#pragma once

//windows
#include <windows.h>
#include <windowsx.h>

//DirectX
#include <d3d9.h>
#include <d3dx9.h>

//Game engine
#include "timer.h"
#include "sound.h"