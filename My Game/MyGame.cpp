/// \file MyGame.cpp 

/// \mainpage Rube Goldberg Prototype

#include "gamedefines.h"
#include "SndList.h"

#include "Box2D\Box2D.h"
#include "contactlistener.h"
#include "RenderWorld.h"
#include "ObjectWorld.h"
#include "Truck.h"

//globals
char g_szGameName[256]; ///< Name of this game.
GameStateType g_nGameState; ///< Current game state.

//So that the player can't continue to keep spawning balls
bool createdStart = false;


//Debug Tools
float worldWidth, worldHeight;
POINT cursorPos;


CTimer g_cTimer; ///< The game timer.
CSoundManager* g_pSoundManager; ///< The sound manager.
b2Body* g_pMoveList[MOVELISTMAX]; //A list of all the objects we need to move after the time step
b2Body* g_pAntiGravList[ANTIGRAVLISTMAX];

//Physics World
b2World g_b2dPhysicsWorld(b2Vec2(0, RW2PW(-980))); ///< Box2D Physics World.
CMyContactListener g_cMyContactListener; ///< Box2D contact listener.

//Render and Object Worlds
CRenderWorld g_cRenderWorld; ///< The Render World.
CObjectWorld g_cObjectWorld; ///< The Object World.

//To build the trucks in the game
Truck g_cTruckFactory;

//For starting the music initially.
bool m_bStartMusic = false;
bool g_bWallPortal = false;
bool g_bCreateCake = false;
bool m_bNeverAgainCreateCake = false;

//prototypes for Windows functions
int DefaultWinMain(HINSTANCE, HINSTANCE, LPSTR, int);
LRESULT CALLBACK DefaultWindowProc(HWND, UINT, WPARAM, LPARAM);

//prototypes for functions in nonplayerobjects.cpp
void CreateWorldEdges();
void CreateStuff();
void CreateBall(float x, float y);
void CreatePortal(float x, float y, float outX, float outY, GameObjectType firstSkin, GameObjectType secondSkin);
void CreateCake(float x, float y);

/// Create all game objects.

void CreateObjects(){
  CreateWorldEdges(); //edges of screen
  CreateStuff(); //stuff
} //CreateObjects

/// Start the game.

void BeginGame(){ 
  g_nGameState = PLAYING_GAMESTATE; //playing state
  g_cTimer.StartLevelTimer(); //starting level now  
  g_cObjectWorld.clear(); //clear old objects
  for (int i=0; i<MOVELISTMAX; i++)
    g_pMoveList[i] = NULL; //NULL out the move list, just in case
  CreateObjects(); //create new objects
  //Start the music now
  m_bStartMusic = true;

} //BeginGame

/// Initialize and start the game.

void InitGame(){
  //set up Render World
  g_cRenderWorld.Initialize(); //bails if it fails
  g_cRenderWorld.LoadImages(); //load images from xml file list

  //set up Physics World with contact listener
  g_b2dPhysicsWorld.SetContactListener(&g_cMyContactListener);

  //set up Object World
  int w, h;
  g_cRenderWorld.GetWorldSize(w, h);
  g_cObjectWorld.SetWorldSize((float)w, (float)h);

  //now start the game
  BeginGame();
} //InitGame

/// Shut down game and release resources.

void EndGame(){
  g_cRenderWorld.End();
} //EndGame

/// Render a frame of animation.

void RenderFrame(){
  if(g_cRenderWorld.BeginScene()){ //start up graphics pipeline
    g_cRenderWorld.DrawBackground(); //draw the background
    g_cObjectWorld.draw(); //draw the objects
    g_cRenderWorld.EndScene(); //shut down graphics pipeline
  } //if
} //RenderFrame

//All of the code for checking values, to react to changes in game status
void Update()
{
  if(g_bWallPortal)
  {
    //We need to create this portal after the gravity button is pressed, so that the ball doesnt run into it on the way down
   CreatePortal(RW2PW(1024), RW2PW(190) + RW2PW(100), RW2PW(700), RW2PW(420), BLUE_PORTAL_LEFT_OBJECT, ORANGE_PORTAL_UP_OBJECT);
   g_bWallPortal = false; //so that we only create it once
  }
  if (!m_bNeverAgainCreateCake)
  {
    if(g_bCreateCake)
    {
      CreateCake(RW2PW(768/2), RW2PW(300));
      CreatePortal(RW2PW(768/2), RW2PW(32), RW2PW(768/2), RW2PW(332), BLUE_PORTAL_DOWN_OBJECT, ORANGE_PORTAL_UP_OBJECT);
    
      g_bCreateCake = false;
      m_bNeverAgainCreateCake = true;
     }
  }
}

/// Process a frame of animation.
/// Called once a frame to animate game objects and take appropriate
/// action if the player has won or lost.

void ProcessFrame(){
  //stuff that gets done on every frame
  g_cTimer.beginframe(); //capture current time
  g_pSoundManager->beginframe(); //no double sounds
  g_cObjectWorld.move(); //move all objects

  //If the sound player exists and we want to start the music...
  if (g_pSoundManager && m_bStartMusic)
    {
      g_pSoundManager->play(RADIO_SOUND, TRUE);
      m_bStartMusic = false; //so that it doesnt happen again
    }
  RenderFrame(); //render a frame of animation
  Update(); //Put things here involved with constant checkers
} //ProcessFrame

/// Keyboard handler.
/// Take the appropriate action when the user mashes a key on the keyboard.
///  \param k Virtual key code for the key pressed
///  \return TRUE if the game is to exit

BOOL KeyboardHandler(WPARAM k){ //keyboard handler

  //keystrokes that work in any state
  if(k ==  VK_ESCAPE)return TRUE; //quit

  //keystrokes while playing
  switch(k){
    case VK_SPACE: 
      if (createdStart == false) //so that the player cannot keep spawning the balls
      {
       createdStart = true; 
        CreateBall(2, 75.0f); // To start the domino effect
      }
      break;

    case VK_BACK: //clear
      g_cObjectWorld.clear(); 
      createdStart = false;
      m_bNeverAgainCreateCake = false;
      g_bCreateCake = false;
      CreateStuff();
      break;

    case VK_TAB:
      g_cObjectWorld.GetWorldSize(worldWidth, worldHeight);
      worldWidth = RW2PW(worldWidth);
      worldHeight = RW2PW(worldHeight);
      GetCursorPos(&cursorPos);

      int x = (int) cursorPos.x;
      int y = (int) cursorPos.y;

      CreateBall(RW2PW(x), worldHeight - RW2PW(y));
      break;

  } //switch

  return FALSE;
} //KeyboardHandler

// Windows functions.
// Dont mess with these unless you really know what you're doing.
// I've written default functions in the Engine library to take
// care of the boring details of Windows housekeeping.

/// Window procedure.
/// Handler for messages from the Windows API. Dont mess with these unless you really know what you're doing.
///  \param h window handle
///  \param m message code
///  \param w parameter for message 
///  \param l second parameter for message
///  \return 0 if message is handled

LRESULT CALLBACK WindowProc(HWND h, UINT m, WPARAM w, LPARAM l){
  return DefaultWindowProc(h, m, w, l);
} //WindowProc

/// Winmain.  
/// Main entry point for this application. Dont mess with these unless you really know what you're doing.
///  \param hI handle to the current instance of this application
///  \param hP unused
///  \param lpC unused 
///  \param nCS specifies how the window is to be shown
///  \return TRUE if application terminates correctly

int WINAPI WinMain(HINSTANCE hI, HINSTANCE hP, LPSTR lpC, int nCS){                  
  return DefaultWinMain(hI, hP, lpC, nCS);
} //WinMain