/// \file particle.h
/// Interface for the particle class CParticle.

#pragma once

#include "gamedefines.h"
#include "sprite.h"
#include "Object.h"

/// \brief The particle object.

class CParticle: public CGameObject{ 
  friend class CObjectManager;
  friend class CParticleEngine;
  friend class CRenderWorld;

  protected:
    D3DXVECTOR2 m_vLocation; ///< Current location.
    float m_fAngle; ///< Current orientation.
    int m_nBirthTime; ///< Time of creation.
    int m_nLifeSpan; ///< Time it will live for.
    float m_fMagnification; ///< Maximum size multiplier.

  public:
    CParticle(GameObjectType t); ///< Constructor
    void Set(D3DXVECTOR2 p, int lifetime, float magnify); ///< Set particle parameters.
}; //CParticle