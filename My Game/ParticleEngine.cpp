/// \file particleengine.cpp
/// Code for the particle engine class CParticleEngine.

#include "ParticleEngine.h"

#include "renderworld.h"

extern CRenderWorld g_cRenderWorld;
extern CTimer g_cTimer;

CParticleEngine::CParticleEngine(int size): CObjectManager(size){
} //constructor

/// Create a particle given its type and lifespan.
/// \param t Object type.
/// \param lifespan Lifespan in ms.

CParticle* CParticleEngine::create(GameObjectType t, int lifespan){
  if(m_nCount < m_nSize){ //if room, create object
    int i=0; while(m_pObjectList[i])i++; //find first free slot
    CParticle* p = new CParticle(t); //create new particle
    p->m_nBirthTime = g_cTimer.time(); //tell it its birth time.
    p->m_nLifeSpan = lifespan; //tell it its lifespan.
    m_pObjectList[i] = p; //put into empty place found in object list.
    m_nCount++; //one more object
    return p;
  } //if
  else return NULL;
} //create

/// Move particles, and cull them if they've exceeded their lifespan.
/// Moving them in fact means rotating them, since they don't actually
/// change their locations.

void CParticleEngine::move(){ 
  for(int i=0; i<m_nSize; i++){ //for each object
    CParticle* p = (CParticle*)m_pObjectList[i]; //handy particle pointer
    if(p){ //if there's a particle there
      float lived; //fraction of life lived, between 0.0 and 1.0
      lived = (g_cTimer.time() - p->m_nBirthTime)/(float)p->m_nLifeSpan; //fraction of life lived
      p->m_fAngle += lived/8.0f; //rotate some   
      if(lived >= 1.0f){ //"He's dead, Dave." --- Holly, on "Red Dwarf"
        delete m_pObjectList[i]; //delete object
        m_pObjectList[i] = NULL; //remove from object list
        m_nCount--; //one less object
      } //if dead
    }//if p
  } //for 
} //move

/// Draw all particles. The particles grow from nothing and rotate in one direction
/// until half their lifespan has passed, then they shrink and rotate the other way
/// until they disappear entirely. At that point the move() function is supposed to
/// cull them. If it fails to cull them, you'll see particles getting very, very big.

void CParticleEngine::draw(){ //draw all particles, with special-effects
  for(int i=0; i<m_nSize; i++){ //for each particle in list
    CParticle* p = (CParticle*)m_pObjectList[i]; //get ptr to it
    if(p){ //if there's a particle there
      float lived = 2.0f * (g_cTimer.time() - p->m_nBirthTime)/(float)p->m_nLifeSpan;
      g_cRenderWorld.draw(p->m_nObjectType,
        p->m_vLocation.x, p->m_vLocation.y, p->m_fAngle, 
         p->m_fMagnification * ((lived < 1.0f)? lived: 2.0f - lived));
    } //if
  } //for
} //draw