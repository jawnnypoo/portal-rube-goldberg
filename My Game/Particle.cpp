/// \file particle.cpp
/// Code for the particle class CParticle.

#include "particle.h"

#include "gamedefines.h" //game defines
#include "spriteman.h" //sprite manager

extern CTimer g_cTimer;

/// Constructor.
/// \param t Object type.

CParticle::CParticle(GameObjectType t): CGameObject(t){ //constructor
  m_vLocation = D3DXVECTOR2(0, 0);
  m_nLifeSpan = 5000;
  m_fMagnification = 1.0f;
  m_fAngle = 0.0f;
  m_nBirthTime = g_cTimer.time();
} //constructor

/// Set particle-related parameter values.
/// \param p Location in Render World.
/// \param lifetime Expected lifetime.
/// \param magnify Maximum size multiplier.

void CParticle::Set(D3DXVECTOR2 p, int lifetime, float magnify){
  m_vLocation = p;
  m_nLifeSpan = lifetime;
  m_fMagnification = magnify;
} //Set