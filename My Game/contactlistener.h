/// \file contactlistener.h
/// Interface for my Box2D contact listeners.

#pragma once

#include "gamedefines.h"
#include "Box2D\Box2D.h"

/// \brief My contact listener.

class CMyContactListener: public b2ContactListener{
  public:
    void PreSolve(b2Contact* contact, const b2Manifold* oldManifold); ///< Box2D runs this before relaxation.
}; //CMyContactListener