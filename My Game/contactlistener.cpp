/// \file contactlistener.cpp
/// Code for my Box2D contact listener.

#include "contactlistener.h"

#include "gamedefines.h"
#include "SndList.h"
#include "objectworld.h"
#include "Truck.h"
#include "Timer.h"

extern CSoundManager* g_pSoundManager;
extern CObjectWorld g_cObjectWorld;
extern b2World g_b2dPhysicsWorld; ///< Box2D Physics World.
extern Truck g_cTruckFactory;
extern CTimer g_cTimer;
extern b2Body* g_pMoveList[MOVELISTMAX];
extern bool g_bWallPortal; //Variable to create the wallportal dynamically
extern bool g_bCreateCake;

/// Presolve function. Renders a colored star at each contact point and plays
/// the appropriate sound, depending on what type of objects are contacting.
/// \param contact Pointer to the contact.
/// \param oldManifold Pointer to the old contact manifold as it was before this contact.

void CMyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold){
  b2WorldManifold worldManifold;
  contact->GetWorldManifold(&worldManifold);
  b2PointState state1[2], state2[2];
  b2GetPointStates(state1, state2, oldManifold, contact->GetManifold());
  for(int i=0; i<2; i++)
    if(state2[i] == b2_addState){
      b2Body* bodyA = contact->GetFixtureA()->GetBody();
      b2Body* bodyB = contact->GetFixtureB()->GetBody();
      b2Fixture* fixtureA = contact->GetFixtureA();
      b2Fixture* fixtureB = contact->GetFixtureB();
      b2Vec2 wp = worldManifold.points[0];
      b2Vec2 vA = bodyA->GetLinearVelocityFromWorldPoint(wp);
      b2Vec2 vB = bodyB->GetLinearVelocityFromWorldPoint(wp);
      b2Vec2 deltavee = vA - vB;
      float32 speed = b2Dot(deltavee, worldManifold.normal);

      //find out what type of objects are contacting
      CGameObject* objectA = (CGameObject*)bodyA->GetUserData();
      CGameObject* objectB = (CGameObject*)bodyB->GetUserData();
      GameObjectType typeA = UNKNOWN_OBJECT;
      GameObjectType typeB = UNKNOWN_OBJECT;
      if(objectA) typeA = (GameObjectType) objectA->m_nObjectType;
      if(objectB) typeB = (GameObjectType) objectB->m_nObjectType;
      
      //collision response
      if(speed > 3.5f)
      { //objects moving fast enough 
        
        if(typeA == BUTTON_OBJECT || typeB == BUTTON_OBJECT)
        {
          g_pSoundManager->play(YAY_SOUND);
          g_bCreateCake = true;
        }
        if(typeA == BALL_OBJECT || typeB == BALL_OBJECT)
          g_pSoundManager->play(THUMP2_SOUND);
        else if(typeA == DOMINO_OBJECT || typeB == DOMINO_OBJECT)
        {
          //Randomly play one of the sounds from the turrets
          int chance = g_cTimer.time() % 10;

          if (chance == 0)
            g_pSoundManager->play(HEY_SOUND);
          else if (chance == 1)
            g_pSoundManager->play(BLAME_SOUND);
          else if (chance == 2)
            g_pSoundManager->play(DIFFERENT_SOUND);
        }
        //When we hit the truck with the ball, start its motor
        if (typeA == TRUCK_BODY_OBJECT && typeB == BALL_OBJECT)
          g_cTruckFactory.MoveIt(100);

        //If we contact a right side entrance portal...
        if (typeA == BLUE_PORTAL_RIGHT_OBJECT && typeB == BALL_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); 
          objectB->m_fXout = objectA->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectB->m_fYout = objectA->m_fYout; //Tell it where the Y value is
          objectB->m_nOut = objectA->m_nOut; //Tell the object coming in which direction the portal they will come out of faces
          //Find a place in the queue of the global move list (transform has to come after the step)
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move & and spot is avalible
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if ((typeA == COMPANION_CUBE_OBJECT && typeB == BLUE_PORTAL_RIGHT_OBJECT))
        {
          g_pSoundManager->play(PORTAL_SOUND);
          objectA->m_fXout = objectB->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectA->m_fYout = objectB->m_fYout; //Tell it where the Y value is
          objectA->m_nOut = objectB->m_nOut; //Tell the object coming in which direction the portal they will come out of faces
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyA;
            }
           }
        }

        if ((typeA == BASKET_OBJECT && typeB == BALL_OBJECT))
        {
          g_pSoundManager->play(SPACE_SOUND); 
        }

        if (typeA == BLUE_PORTAL_DOWN_OBJECT && typeB == BALL_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); 
          objectB->m_fXout = objectA->m_fXout; 
          objectB->m_fYout = objectA->m_fYout; 
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if (typeA == BLUE_PORTAL_DOWN_OBJECT && typeB == CAKE_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); 
          objectB->m_fXout = objectA->m_fXout; 
          objectB->m_fYout = objectA->m_fYout; 
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if (typeA == CAKE_OBJECT && typeB == BLUE_PORTAL_DOWN_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); 
          objectA->m_fXout = objectB->m_fXout; 
          objectA->m_fYout = objectB->m_fYout; 
          objectA->m_nOut = objectB->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyA;
            }
           }
        }

        if (typeA == BLUE_PORTAL_LEFT_OBJECT && typeB == BALL_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND);
          objectB->m_fXout = objectA->m_fXout; 
          objectB->m_fYout = objectA->m_fYout; 
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if (typeA == BLUE_PORTAL_UP_OBJECT && typeB == SPACE_CORE_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); //Replace with going through portal sound
          objectB->m_fXout = objectA->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectB->m_fYout = objectA->m_fYout; //Tell it where the Y value is
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if (typeA == BLUE_PORTAL_DOWN_OBJECT && typeB == SPACE_CORE_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); //Replace with going through portal sound
          objectB->m_fXout = objectA->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectB->m_fYout = objectA->m_fYout; //Tell it where the Y value is
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }

        if (typeA == COMPANION_CUBE_OBJECT && typeB == BLUE_PORTAL_DOWN_OBJECT)
        {
          objectA->m_fXout = objectB->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectA->m_fYout = objectB->m_fYout; //Tell it where the Y value is
          objectA->m_nOut = objectB->m_nOut; //Tell the object coming in which direction the portal they will come out of faces
          for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyA;
            }
           }
        }

        if (typeA == BLUE_PORTAL_RIGHT_OBJECT && typeB == SPACE_CORE_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); //Replace with going through portal sound
          objectB->m_fXout = objectA->m_fXout; //Tell the object that collided with the portal where the X value of the out portal is
          objectB->m_fYout = objectA->m_fYout; //Tell it where the Y value is
          objectB->m_nOut = objectA->m_nOut;
           for (int i=0; i<MOVELISTMAX; i++)
           {
            //If we have an object to move
            if(g_pMoveList[i] == NULL)
            {
              g_pMoveList[i] = bodyB;
            }
           }
        }
        //For when the truck hits the gravity button
        if (typeA == GRAVITY_BUTTON_OBJECT && typeB == WHEEL_OBJECT)
        {
          g_pSoundManager->play(PORTAL_SOUND); //Replace with going through portal sound
          //Flip the truck so the ball comes flying out
          g_cTruckFactory.FlipIt();
          //Stop the truck motor
          g_cTruckFactory.StopIt();
          //Portal that catches the ball when hitting wall, shooting them into the basket
          g_bWallPortal = true;
        }
        //star
        CParticle* pParticle = g_cObjectWorld.create(BLUESTAR_OBJECT, 500);
        if(pParticle)pParticle->      
          Set(D3DXVECTOR2(PW2RW(wp.x), PW2RW(wp.y)), 500, 1.0f);
      } //if
  } //if
    
} //PreSolve