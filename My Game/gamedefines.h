/// \file gamedefines.h
/// Game defines.

#pragma once

#include "defines.h"

/// \brief Object type.
///
/// Types of object that can appear in the game. 

enum GameObjectType{
  UNKNOWN_OBJECT, BALL_OBJECT, DOMINO_OBJECT, BUTTON_OBJECT, BASKET_OBJECT, WHEEL_OBJECT, TRUCK_BODY_OBJECT,
  BLUE_PORTAL_LEFT_OBJECT, BLUE_PORTAL_RIGHT_OBJECT, ORANGE_PORTAL_LEFT_OBJECT, ORANGE_PORTAL_RIGHT_OBJECT, BLUE_PORTAL_UP_OBJECT,
  BLUE_PORTAL_DOWN_OBJECT, ORANGE_PORTAL_UP_OBJECT, COMPANION_CUBE_OBJECT, SPACE_CORE_OBJECT, GRAVITY_BUTTON_OBJECT, CAKE_OBJECT,

  WHITESTAR_OBJECT, YELLOWSTAR_OBJECT, REDSTAR_OBJECT,
  MAGENTASTAR_OBJECT, BLUESTAR_OBJECT
}; //GameObjectType

//So we can store what kind of the direction the ball will come out of the portal
enum OutType
{
  NONE, LEFT, RIGHT, UP, DOWN, NUM_OUT_TYPES
};

//Translate units between Render World and Physics World
const float fPRV = 10.0f; ///< Physics world rescale value
inline float PW2RW(float x){return x*fPRV;}; ///< Physics World to Render World units.
inline float RW2PW(float x){return x/fPRV;}; ///< Render World to Physics World units.
inline float RW2PW(int x){return (float)x/fPRV;}; ///< Render World to Physics World units.
//Max number of objects to store in the move list, shouldnt need to be too many
const int MOVELISTMAX = 4;
const int ANTIGRAVLISTMAX = 10;
/// State of game play, including whether the player has won or lost.

enum GameStateType{
  PLAYING_GAMESTATE, WON_GAMESTATE, LOST_GAMESTATE
}; //GameStateType