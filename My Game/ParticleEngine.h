/// \file particleengine.h
/// Interface for the particle engine class CParticleEngine.

#pragma once

#include "gamedefines.h"
#include "ObjectManager.h"
#include "particle.h"

/// \brief The particle manager.
///
/// The particle engine is responsible for managing particles.

class CParticleEngine: public CObjectManager{
  public:
    CParticleEngine(int size); ///< Constructor.
    CParticle* create(GameObjectType t, int lifespan); //< Create particle.
    void move(); ///< Move all particles.
    void draw(); ///< Draw all particles
}; //CParticleEngine
