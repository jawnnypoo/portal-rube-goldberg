/// \file object.h
/// Interface for the game object class CGameObject. Game Objects live in Object World.

#pragma once

#include "gamedefines.h"
#include "Box2D\Box2D.h"

/// \brief The game object.
///
/// Game objects are responsible for remembering information about themselves,
/// in particular, their representations in Render World and Physics World.

class CGameObject{ //class for a game object

  //CGameObject has a lot of friends
  friend class CObjectManager; //because the Object World manages objects
  friend class CHeadsUpDisplay; //because the HUD has objects in it too
  friend class CObjectWorld; //...and the Object World too, obviously
  friend class CRenderWorld; //because Render World need to draw them
  friend class CMyContactListener; //because this is how Physics World messes with them

  protected:
    GameObjectType m_nObjectType; ///< Object type.
    b2Body* m_pBody; ///< Physics World body.
    

  public:
    CGameObject(GameObjectType objecttype); ///< Constructor.
    ~CGameObject(); ///< Destructor.
    void SetPhysicsBody(b2Body* b); ///< Set pointer to physics world body.
    float m_fXout; //For the portal, you tell where to come out when going through
    float m_fYout; //This tells the Y value of where to come out of the portal
    OutType m_nOut; //The direction the object is to travel when it comes out of the portal

}; //CGameObject