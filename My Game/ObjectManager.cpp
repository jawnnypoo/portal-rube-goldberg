/// \file ObjectManager.cpp
/// Code for the object manager class CObjectManager.

#include "ObjectManager.h"

#include "gamedefines.h"
#include "renderworld.h"

extern b2World g_b2dPhysicsWorld;
extern CRenderWorld g_cRenderWorld;
extern b2Body* g_pMoveList[MOVELISTMAX];

CObjectManager::CObjectManager(int size){ //constructor 
  m_pObjectList = new CGameObject* [size];
  m_nSize = size;
  m_nCount = 0; //no objects
  for(int i=0; i<size; i++) //null our object list
    m_pObjectList[i] = NULL;
} //constructor

CObjectManager::~CObjectManager(){ //destructor
  for(int i=0; i<m_nSize; i++) //for each object
    delete m_pObjectList[i]; //delete it
  delete [] m_pObjectList;
} //destructor

/// Create a new instance of a game object.
/// \param objecttype The type of the new object

CGameObject* CObjectManager::create(GameObjectType objecttype){
  if(m_nCount < m_nSize){ //if room, create object
    int i=0; while(m_pObjectList[i])i++; //find first free slot
    m_pObjectList[i] = new CGameObject(objecttype);
    m_nCount++; //one more object
    return m_pObjectList[i];
  } //if
  else return NULL;
} //create

/// Ask the Physics World to move all game objects.

void CObjectManager::move(){ 
  g_b2dPhysicsWorld.Step(1.0f/60.0f, 6, 2);
  TransformStuff();
} //move

/// Ask the Render World to draw all game objects.

void CObjectManager::draw(){ 
  CGameObject* p;
  for(int i=0; i<m_nSize; i++){ //for each object slot
    p = m_pObjectList[i]; //handy object pointer
    if(p){ //if there's an object there
      float a = p->m_pBody->GetAngle(); //orientation
      b2Vec2 v = p->m_pBody->GetPosition(); //position in Physics World units
      g_cRenderWorld.draw(p->m_nObjectType, PW2RW(v.x), PW2RW(v.y), a);
    } //if
  } //for
} //draw

/// Clear out all game objects from the object list. 

void CObjectManager::clear(){ 
  m_nCount = 0; //no objects
  for(int i=0; i<m_nSize; i++){ //for each object
    delete m_pObjectList[i]; //delete it
    m_pObjectList[i] = NULL; //safe delete
  } //for
} //clear

//This function is called after the Step function, so that we can move objects without
//interupting their physics calculations. Could also be used to destroy objects
void CObjectManager::TransformStuff()
{
  for (int i = 0; i<MOVELISTMAX; i++)
  {
    //If we have an object to move
    if(g_pMoveList[i])
    {
      //We first get the object from the body
      CGameObject* object = (CGameObject*)g_pMoveList[i]->GetUserData();
      //Switch Statement based on where we apply a force to get the object moving faster when exiting the portal
      //Also allows us to move the object just enough so that we dont hit the out portal
      b2Vec2 force;
      float x = 0.0f; //the amout we move the x and y so that we dont hit the out portal
      float y = 0.0f;
      switch (object->m_nOut)
      {
        case LEFT:
          force = b2Vec2(-100.0f, 10.0f);
          x = -RW2PW(16); //start at the left side of the out portal
          break;
        case RIGHT:
          force = b2Vec2(100.0f, 10.0f);
          x = RW2PW(16); //start at the right side of the out portal
          break;
        case DOWN:
          force = b2Vec2(0.0f,0.0f); //Don't speed up these objects
          y = -RW2PW(16);
          break;
            
      }

      //We apply the transform to the object based on the coordinates we passed its object
      //when the collision occured
      g_pMoveList[i]->SetTransform(b2Vec2(object->m_fXout + x, object->m_fYout + y), g_pMoveList[i]->GetAngle());

      b2Vec2 point = g_pMoveList[i]->GetPosition(); //We want to apply the force to the center of mass
      g_pMoveList[i]->ApplyLinearImpulse(force, point);

      //Set it back to NULL since we do not need to transform this object again
      g_pMoveList[i] = NULL;
    }
  }

}