/// \file objectworld.h
/// Interface for the ObjectWorld class CObjectWorld.

#pragma once

#include "ObjectManager.h"
#include "ParticleEngine.h"

/// \brief The Object World.

BOOL KeyboardHandler(WPARAM k); //to enable easter egg

class CObjectWorld{
  friend BOOL KeyboardHandler(WPARAM k); //to enable easter egg

  private:
    CObjectManager* m_pObjectManager; ///< Object manager.
    CParticleEngine* m_pParticleEngine; ///< Particle engine.
    float m_fWidth, m_fHeight; ///< Object world width and height.

  public:
    CObjectWorld(); //< Constructor.
    ~CObjectWorld(); //< Destructor.

    CParticle* create(GameObjectType t, int lifespan); //< Create new particle.
    CGameObject* create(GameObjectType t); ///< Create new object.

    void SetWorldSize(float width, float height); ///< Set the Object World width and depth.
    void GetWorldSize(float &width, float &height); ///< Get the Object World width and depth.

    void clear(); //< Reset to initial conditions.
    void move(); //< Move all objects and particles
    void draw(); //< Draw all objects and particles.
};