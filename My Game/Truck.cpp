//Code for creation of the Truck

#include "Truck.h"
#include "ObjectWorld.h"
#include "gamedefines.h"

extern CObjectWorld g_cObjectWorld;
extern b2World g_b2dPhysicsWorld;
extern CTimer g_cTimer;


//Constructor for the truck
Truck::Truck()
{
  m_pTruckBody = NULL;
  m_pWheel1 = m_pWheel2 = NULL;
  m_pWheelJoint1 = m_pWheelJoint2 = NULL;
  //Reset();
} 

b2Body* Truck::CreateTruckBody(int x, int y, int index, CGameObject* object)
{
  //Shape of the object


	//Custom object created by three boxes. We center them relative to the 
	//body position center, which should be in the very center of the size 
	//of the original PNG image file
	b2PolygonShape bottom;
	bottom.SetAsBox(RW2PW(48), RW2PW(8), b2Vec2(0.0f, -RW2PW(24)), 0);
	b2PolygonShape left;
	left.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(-RW2PW(56), 0.0f), 0);

	b2PolygonShape right;
	right.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(RW2PW(56), 0.0f), 0);

	b2BodyDef bd;
	bd.type = b2_dynamicBody;
	bd.position.Set(RW2PW(x), RW2PW(y));
	bd.userData = object;
	b2Body* body = g_b2dPhysicsWorld.CreateBody(&bd);
	body->CreateFixture(&bottom,1.0f);
	body->CreateFixture(&left,1.0f);
	body->CreateFixture(&right,1.0f);
  return body;
}

b2Body* Truck::CreateWheel(int x, int y, int index, CGameObject *object)
{
  b2CircleShape shape;
	shape.m_radius = RW2PW(32);

  //fixture
	b2FixtureDef fd;
	fd.shape = &shape;
	fd.density = 0.8f;
	fd.restitution = 0.6f;
  fd.filter.groupIndex = index;

  //body definition
  b2BodyDef bd;
	bd.type = b2_dynamicBody;
  bd.position.Set(RW2PW(x), RW2PW(y)); 
  bd.userData = object;

  //body
  b2Body* body;
  body = g_b2dPhysicsWorld.CreateBody(&bd);
  body->CreateFixture(&fd);
  return body;
}

void Truck::Create(float x, float y)
{
  const int index = -42;
  //where we will spawn the truck x and y

  CGameObject* pTruckBody = g_cObjectWorld.create(TRUCK_BODY_OBJECT);
  CGameObject* pWheel1 = g_cObjectWorld.create(WHEEL_OBJECT);
  CGameObject* pWheel2 = g_cObjectWorld.create(WHEEL_OBJECT);

  m_pTruckBody = CreateTruckBody(x, y+80, index, pTruckBody); //Adjust y for the body to be above the wheels
  m_pWheel1 = CreateWheel(x - 60, y, index, pWheel1);
  m_pWheel2 = CreateWheel(x + 60, y, index, pWheel2);

  //wheel joint definition
  b2WheelJointDef wd;
  b2Vec2 axis(0.0f, 0.9f); //vertical axis for wheel suspension
  wd.Initialize(m_pTruckBody, m_pWheel1, m_pWheel1->GetPosition(), axis);
  wd.dampingRatio = 0.9f;
  wd.motorSpeed = 0.0f;
  wd.maxMotorTorque = 1000.0f;
  wd.enableMotor = TRUE;

  //create wheel joint for wheel 1
  m_pWheelJoint1 = (b2WheelJoint*)g_b2dPhysicsWorld.CreateJoint(&wd);

  //Now for wheel 2
  wd.Initialize(m_pTruckBody, m_pWheel2, m_pWheel2->GetPosition(), axis);
  m_pWheelJoint2 = (b2WheelJoint*)g_b2dPhysicsWorld.CreateJoint(&wd);

  //tell Object World cannon parts about Physics World counterparts
  pTruckBody->SetPhysicsBody(m_pTruckBody);
  pWheel1->SetPhysicsBody(m_pWheel1);
  pWheel2->SetPhysicsBody(m_pWheel2);
}

//Moving set by speed, where + is left and - is right
void Truck::MoveIt(float speed)
{
  if(m_pWheelJoint1)
  {
    m_pWheelJoint1->SetMotorSpeed(-speed);
    m_pWheelJoint1->EnableMotor(TRUE);
  } //Get one wheel moving

  if(m_pWheelJoint2)
  {
    m_pWheelJoint2->SetMotorSpeed(-speed);
    m_pWheelJoint2->EnableMotor(TRUE); 
  } //Now move the other

} 

//Stop the truck from moving
void Truck::StopIt()
{
  if(m_pWheelJoint1)
  {
    m_pWheelJoint1->SetMotorSpeed(0.0f);
    m_pWheelJoint1->EnableMotor(FALSE);
  }

  if(m_pWheelJoint2)
  {
    m_pWheelJoint2->SetMotorSpeed(0.0f);
    m_pWheelJoint2->EnableMotor(FALSE); 
  }
} 

void Truck::FlipIt()
{
  m_pTruckBody->ApplyLinearImpulse(b2Vec2(-200, 0), m_pTruckBody->GetLocalCenter());
}
