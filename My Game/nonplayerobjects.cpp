/// \file nonplayerobjects.cpp

#include "gamedefines.h"
#include "Box2D\Box2D.h"
#include "ObjectWorld.h"
#include "Truck.h"

extern b2World g_b2dPhysicsWorld;
extern CObjectWorld g_cObjectWorld;
extern Truck g_cTruckFactory;


//Create a cube (square, really)
void CreateCompanionCube(float x, float y)
{ 

  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(COMPANION_CUBE_OBJECT);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd; 
	bd.type = b2_dynamicBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body
  //shape
  b2PolygonShape blockshape;
  blockshape.SetAsBox(RW2PW(16), RW2PW(16));

  //fixture
  b2FixtureDef blockfd;
	blockfd.shape = &blockshape;
	blockfd.density = 0.2f;
	blockfd.restitution = 0.0f;

  //body
  b2Body* pBlock = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBlock); //tell object world body about physics world body
  pBlock->CreateFixture(&blockfd);
} //CreateCube

//Tell the function where you want the basket, and it places it there. Same as the truck bed
void CreateBasket(float x, float y)
{
  CGameObject* pGameObject = g_cObjectWorld.create(BASKET_OBJECT);
  if (pGameObject == NULL)return;
	//Custom object created by three boxes. We center them relative to the 
	//body position center
	b2PolygonShape bottom;
	bottom.SetAsBox(RW2PW(48), RW2PW(8), b2Vec2(0.0f, -RW2PW(24)), 0);
	b2PolygonShape left;
	left.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(-RW2PW(56), 0.0f), 0);

	b2PolygonShape right;
	right.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(RW2PW(56), 0.0f), 0);

	b2BodyDef bd;
  bd.type = b2_staticBody;
	bd.position.Set(x, y);
	bd.userData = (void*)pGameObject; //tell physics world body about object world body
	b2Body* body = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(body);
	body->CreateFixture(&bottom,1.0f);
	body->CreateFixture(&left,1.0f);
	body->CreateFixture(&right,1.0f);
}

//The space core object
void CreateSpaceCore(float x, float y)
{ 

  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(SPACE_CORE_OBJECT);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd;
	bd.type = b2_dynamicBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body

  //shape
  b2CircleShape ballshape;
	ballshape.m_radius = RW2PW(32);
	b2FixtureDef ballfd;

  //fixture
	ballfd.shape = &ballshape;
	ballfd.density = 2.0f;
	ballfd.restitution = 0.3f;

  //body
  b2Body* pBall = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBall); //tell object world body about physics world body
  pBall->CreateFixture(&ballfd);
} //CreateBall

void CreatePortal(float x, float y, float outX, float outY, GameObjectType firstSkin, GameObjectType secondSkin)
{
  //Object world creaetion
  CGameObject* pBluePortalObject = g_cObjectWorld.create(firstSkin);
  if(pBluePortalObject == NULL) return;

  CGameObject* pOrangePortalObject = g_cObjectWorld.create(secondSkin);
  if(pOrangePortalObject == NULL) return;

  //Building the Blue Portal

  //Physics World
  b2BodyDef bd; 
  bd.type = b2_staticBody; //We don't want reactions 
  bd.position.Set(x, y); //Set the position in world coordinates
  bd.userData = (void*)pBluePortalObject; //tell physics world body about object world body

  //shape
  b2PolygonShape blockshape;
  //If our portal is standing up
  if (firstSkin == BLUE_PORTAL_LEFT_OBJECT || firstSkin == BLUE_PORTAL_RIGHT_OBJECT)
    blockshape.SetAsBox(RW2PW(16), RW2PW(64)); //Making a rectangle to house the portals
  //If our portal is laying down
  if (firstSkin == BLUE_PORTAL_DOWN_OBJECT)
    blockshape.SetAsBox(RW2PW(64), RW2PW(16)); //Making a rectangle to house the portals
  //fixture
  b2FixtureDef blockfd;
	blockfd.shape = &blockshape;
	blockfd.density = 100.0f;
	blockfd.restitution = 0.0f;

  //body
  b2Body* pInPortal = g_b2dPhysicsWorld.CreateBody(&bd);
  pBluePortalObject->SetPhysicsBody(pInPortal); //tell object world body about physics world body
  pInPortal->CreateFixture(&blockfd);

  pBluePortalObject->m_fXout = outX; //Kludge so that we don't crash into the out portal
  pBluePortalObject->m_fYout = outY;
  //We find out what the direction the orange portal is facing, so that we can pass that info
  //on to the object that goes into the blue portal, allowing the object to translate its speed
  switch (secondSkin)
  {
  case ORANGE_PORTAL_RIGHT_OBJECT:
    pBluePortalObject->m_nOut = RIGHT;
    break;
  case ORANGE_PORTAL_LEFT_OBJECT:
    pBluePortalObject->m_nOut = LEFT;
    break;
  case ORANGE_PORTAL_UP_OBJECT:
    pBluePortalObject->m_nOut = DOWN; 
    break;

  }
  //Done with the Blue Portal

  //Building the orange portal

  //Physics World
  b2BodyDef bd1; 
  bd1.type = b2_staticBody;
  bd1.position.Set(outX, outY);
  bd1.userData = (void*)pOrangePortalObject; //tell physics world body about object world body

  //shape
  b2PolygonShape blockshape1;
  if (secondSkin == ORANGE_PORTAL_LEFT_OBJECT || secondSkin == ORANGE_PORTAL_RIGHT_OBJECT)
    blockshape1.SetAsBox(RW2PW(16), RW2PW(64)); //Making a rectangle to house the portals
  if (secondSkin == ORANGE_PORTAL_UP_OBJECT)
    blockshape1.SetAsBox(RW2PW(64), RW2PW(16)); //Making a rectangle to house the portals

  //fixture
  b2FixtureDef blockfd1;
	blockfd1.shape = &blockshape1;
	blockfd1.density = 100.0f;
	blockfd1.restitution = 0.0f;

  //body
  b2Body* pOutPortal = g_b2dPhysicsWorld.CreateBody(&bd1);
  pOrangePortalObject->SetPhysicsBody(pOutPortal); //tell object world body about physics world body
  pOutPortal->CreateFixture(&blockfd1);
}

/// Place a domino in Physics World and Object World.
/// \param x Horizontal coordinate in Physics World units.
/// \param y  Vertical coordinate in Physics World units.

void CreateDomino(float x, float y)
{ 

  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(DOMINO_OBJECT);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd; 
	bd.type = b2_dynamicBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body
  //shape
  b2PolygonShape blockshape;
  blockshape.SetAsBox(RW2PW(5), RW2PW(32));

  //fixture
  b2FixtureDef blockfd;
	blockfd.shape = &blockshape;
	blockfd.density = 10.0f;
	blockfd.restitution = 0.0f;

  //body
  b2Body* pBlock = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBlock); //tell object world body about physics world body
  pBlock->CreateFixture(&blockfd);
} //CreateDomino

/// Place a ball in Physics World and Object World.
/// \param x Horizontal coordinate in Physics World units.
/// \param y  Vertical coordinate in Physics World units.

void CreateBall(float x, float y)
{ 

  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(BALL_OBJECT);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd;
	bd.type = b2_dynamicBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body

  //shape
  b2CircleShape ballshape;
	ballshape.m_radius = RW2PW(16);
	b2FixtureDef ballfd;

  //fixture
	ballfd.shape = &ballshape;
	ballfd.density = 2.0f;
	ballfd.restitution = 0.3f;
  //ballfd.filter.groupIndex = -42; //We don't want the ball to run into the fake truck frame

  //body
  b2Body* pBall = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBall); //tell object world body about physics world body
  pBall->CreateFixture(&ballfd);
} //CreateBall

/// Create world edges in Physics World.
/// Place Box2D edge shapes in the Physics World in places that correspond to the
/// top, bottom, right, and left edges of the screen in Render World.

void CreateWorldEdges()
{
  float w, h;
  g_cObjectWorld.GetWorldSize(w, h);

  //Box2D ground
  b2BodyDef bd;
	b2Body* edge = g_b2dPhysicsWorld.CreateBody(&bd);
  b2EdgeShape shape;
	shape.Set(b2Vec2(0, 0), 
    b2Vec2(RW2PW(w), 0));
	edge->CreateFixture(&shape, 0);

  //Box2D left edge of screen
  edge = g_b2dPhysicsWorld.CreateBody(&bd);
  shape.Set(b2Vec2(0, RW2PW(-1000)), b2Vec2(0, RW2PW(1000)));
	edge->CreateFixture(&shape, 0);

  //Box2D right edge of screen
  bd.position.x = RW2PW(w);
  edge = g_b2dPhysicsWorld.CreateBody(&bd);
	edge->CreateFixture(&shape, 0); 
} //CreateWorldEdges

//Use this function both for the end button and to create our gravity buttons
void CreateButton(float x, float y, GameObjectType object)
{
  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(object);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd; 
	bd.type = b2_staticBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body

  //shape
  b2PolygonShape blockshape;
  blockshape.SetAsBox(RW2PW(16), RW2PW(16));

  //fixture
  b2FixtureDef blockfd;
	blockfd.shape = &blockshape;
	blockfd.density = 10.0f;
	blockfd.restitution = 0.2f;

  //body
  b2Body* pBlock = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBlock); //tell object world body about physics world body
  pBlock->CreateFixture(&blockfd);

} //CreateButton

void CreateCake(float x, float y)
{
  //Object World
  CGameObject* pGameObject = g_cObjectWorld.create(CAKE_OBJECT);
  if(pGameObject == NULL)return; //bail and fail

  //Physics World
  b2BodyDef bd; 
	bd.type = b2_dynamicBody;
  bd.position.Set(x, y);
  bd.userData = (void*)pGameObject; //tell physics world body about object world body
  //shape
  b2PolygonShape blockshape;
  blockshape.SetAsBox(RW2PW(32), RW2PW(32));

  //fixture
  b2FixtureDef blockfd;
	blockfd.shape = &blockshape;
	blockfd.density = 1.0f;
	blockfd.restitution = 0.0f;

  //body
  b2Body* pBlock = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject->SetPhysicsBody(pBlock); //tell object world body about physics world body
  pBlock->CreateFixture(&blockfd);
}

void CreatePulley(float leftX, float leftY, float rightX, float rightY)
{
  CGameObject* pGameObject1 = g_cObjectWorld.create(BASKET_OBJECT);
  if (pGameObject1 == NULL)return;
	//Custom object created by three boxes. We center them relative to the 
	//body position center
	b2PolygonShape bottom;
	bottom.SetAsBox(RW2PW(48), RW2PW(8), b2Vec2(0.0f, -RW2PW(24)), 0);
	b2PolygonShape left;
	left.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(-RW2PW(56), 0.0f), 0);

	b2PolygonShape right;
	right.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(RW2PW(56), 0.0f), 0);

	b2BodyDef bd;
  bd.type = b2_dynamicBody;
	bd.position.Set(leftX, leftY);
  bd.fixedRotation = true;
	bd.userData = (void*)pGameObject1; //tell physics world body about object world body
	b2Body* bodyA = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject1->SetPhysicsBody(bodyA);
	bodyA->CreateFixture(&bottom,1.0f);
	bodyA->CreateFixture(&left,1.0f);
	bodyA->CreateFixture(&right,1.0f);

  CGameObject* pGameObject2 = g_cObjectWorld.create(BASKET_OBJECT);
  if (pGameObject2 == NULL)return;
	//Custom object created by three boxes. We center them relative to the 
	//body position center
	bottom.SetAsBox(RW2PW(48), RW2PW(8), b2Vec2(0.0f, -RW2PW(24)), 0);
	left.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(-RW2PW(56), 0.0f), 0);

	right.SetAsBox(RW2PW(8), RW2PW(32), b2Vec2(RW2PW(56), 0.0f), 0);

  bd.type = b2_dynamicBody;
	bd.position.Set(rightX, rightY);
  bd.fixedRotation = true;
	bd.userData = (void*)pGameObject2; //tell physics world body about object world body
	b2Body* bodyB = g_b2dPhysicsWorld.CreateBody(&bd);
  pGameObject2->SetPhysicsBody(bodyB);
	bodyB->CreateFixture(&bottom,1.0f);
	bodyB->CreateFixture(&left,1.0f);
	bodyB->CreateFixture(&right,1.0f);

  // Pulley Setup
  b2Vec2 anchor1 = bodyA->GetWorldCenter();
  b2Vec2 anchor2 = bodyB->GetWorldCenter();
  //Set anchors at points above the two baskets
  b2Vec2 groundAnchor1(leftX, leftY + RW2PW(300));
  b2Vec2 groundAnchor2(rightX, rightY + RW2PW(10));
  float ratio = 0.5f;
  b2PulleyJointDef jointDef;
  jointDef.Initialize(bodyA, bodyB, groundAnchor1, groundAnchor2, anchor1, anchor2, ratio);
  b2PulleyJoint* m_joint = (b2PulleyJoint*)g_b2dPhysicsWorld.CreateJoint(&jointDef);
}

void CreateStuff()
{
  float worldWidth, worldHeight;
  g_cObjectWorld.GetWorldSize(worldWidth, worldHeight);
  worldWidth = RW2PW(worldWidth);
  worldHeight = RW2PW(worldHeight);

  b2BodyDef bd;
	b2Body* edge = g_b2dPhysicsWorld.CreateBody(&bd);
  b2EdgeShape shape;

  //shelf
  const float GAP = RW2PW(64); //Gap between right wall and shelf
  const float GAP2 = RW2PW(250); //Gap for the second shelf
  const float GAP3 = RW2PW(700); //Gap for the third shelf
  const float SHELFHT = RW2PW(650);
  const float SHELFHT2 = SHELFHT - RW2PW(200); //second shelf height
  const float SHELFHT3 = SHELFHT2 - RW2PW(260);

  //Creates the first shelf with the dominos on it
  shape.Set(b2Vec2(0, SHELFHT+RW2PW(20)), b2Vec2(GAP, SHELFHT));
  edge->CreateFixture(&shape, 0);

  //Create the second level shelf
  //The Y value is set to be 150 pixels less than the first shelf, which is where the second shelf image is drawn
  shape.Set(b2Vec2(0, SHELFHT2), b2Vec2(worldWidth-GAP2, SHELFHT2)); 
  edge->CreateFixture(&shape, 0);

  //Create the third level shelf
  shape.Set(b2Vec2(0, SHELFHT3), b2Vec2(worldWidth-GAP3, SHELFHT3)); 
  edge->CreateFixture(&shape, 0);

  //slopes to redirect cannonball
  shape.Set(b2Vec2(0, SHELFHT), b2Vec2(worldWidth -GAP, SHELFHT));
  edge->CreateFixture(&shape, 0);

  //Slope on "floor"
  shape.Set(b2Vec2(worldWidth - RW2PW(64), 0), b2Vec2(worldWidth, RW2PW(64)));
  edge->CreateFixture(&shape, 0);

  float cubeWidth = 64;
  //Companion cube stack
  for(int i=0; i<10; i++)
  {
    //Bottom 4
    if (i <= 3)
      CreateCompanionCube(RW2PW(100 + i * 32), SHELFHT3 + RW2PW(32)); //Cube width
    //Bottom 3
    if (i > 3 && i < 7)
      CreateCompanionCube(RW2PW(-20 + i * 32), SHELFHT3 + RW2PW(96)); //Cube width
    //Middle two
    if (i == 7 || i == 8)
      CreateCompanionCube(RW2PW(-100 + i * 32), SHELFHT3 + RW2PW(128)); //add about 64 to the original
    //last one
    if(i == 9)
      CreateCompanionCube(RW2PW(80 + 64), SHELFHT3 + RW2PW(160)); //64 more
      
  }

  //dominos
  for(int i=0; i<20; i++)
    CreateDomino(RW2PW(100 + i*40), SHELFHT+RW2PW(31));

  //cannonball on shelf edge
  CreateBall(worldWidth - GAP - RW2PW(10), SHELFHT);

  //finish button
  CreateButton(RW2PW(16), RW2PW(16), BUTTON_OBJECT);

  g_cTruckFactory.Create(300, 550);
  //Portal on the floor, that brings you up to the second level
  CreatePortal(RW2PW(100), RW2PW(50), RW2PW(32),RW2PW(550), BLUE_PORTAL_RIGHT_OBJECT, ORANGE_PORTAL_RIGHT_OBJECT);
  //Portals on the second level, one on the ground, one on the ceiling to get you in the truck
  CreatePortal(RW2PW(128), RW2PW(450), RW2PW(350),RW2PW(630), BLUE_PORTAL_DOWN_OBJECT, ORANGE_PORTAL_UP_OBJECT);
  //Portal on the 3rd row, to drop you onto the button
  CreatePortal(RW2PW(16), SHELFHT3 + RW2PW(100), RW2PW(60), SHELFHT3 - RW2PW(20), BLUE_PORTAL_RIGHT_OBJECT, ORANGE_PORTAL_UP_OBJECT); 

  //Portal you use to smash the pyramid
  CreatePortal(RW2PW(500), SHELFHT2 - RW2PW(32), RW2PW(200), SHELFHT3 + RW2PW(100), BLUE_PORTAL_UP_OBJECT, ORANGE_PORTAL_LEFT_OBJECT);
  
  CreateSpaceCore(RW2PW(500), SHELFHT2 - RW2PW(236));
  CreateButton(RW2PW(700), SHELFHT2 - RW2PW(2), GRAVITY_BUTTON_OBJECT);
  CreatePulley(RW2PW(500), SHELFHT2 - RW2PW(300), RW2PW(700), SHELFHT2 - RW2PW(100));
} //CreateStuff
