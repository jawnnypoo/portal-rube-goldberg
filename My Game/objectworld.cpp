#include "objectworld.h"
#include "Timer.h"

extern CTimer g_cTimer;

CObjectWorld::CObjectWorld(){
  m_pObjectManager = new CObjectManager(256);
  m_pParticleEngine = new CParticleEngine(1024);
} //constructor

CObjectWorld::~CObjectWorld(){
  delete m_pObjectManager;
  delete m_pParticleEngine;
} //destructor

/// Create a particle in the Object World.
/// \param t Object type.
/// \param lifespan Amount of time that particle is to live from creation.
/// \return Pointer to the particle created.

CParticle* CObjectWorld::create(GameObjectType t, int lifespan){ // Create particle.  
  return m_pParticleEngine->create(t, lifespan);
} //create

/// Create an object in the Object World.
/// \param t Object type.
/// \return Pointer to the object created.

CGameObject* CObjectWorld::create(GameObjectType t){ // Create new object.
  return m_pObjectManager->create(t);
} //create

/// Set the Object World's width and height.
/// \param width Object World width.
/// \param height Object World height.

void CObjectWorld::SetWorldSize(float width, float height){
  m_fWidth = width; m_fHeight = height;
} //SetWorldSize

/// Get the Object World's width and height.
/// \param width Calling parameter will be set to Object World width.
/// \param height Calling parameter will be set to Object World height.

void CObjectWorld::GetWorldSize(float& width, float& height){
  width = m_fWidth; height = m_fHeight;
} //SetWorldSize

/// Clear objects and particles.

void CObjectWorld::clear(){
  m_pObjectManager->clear();
  m_pParticleEngine->clear();

} //clear

/// Draw everything in the Object World.
/// Draw the game objects, then the particles, then the HUD in that order using Painter's
/// Algorithm.

void CObjectWorld::draw(){
  m_pObjectManager->draw(); //draw objects next
  m_pParticleEngine->draw(); //draw particles on top 
} //draw

/// Move objects and particles.

void CObjectWorld::move(){
  m_pObjectManager->move(); //move objects
  m_pParticleEngine->move(); //move particles
} //draw
