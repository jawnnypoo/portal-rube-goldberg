/// \file object.cpp
/// Code for the game object class CGameObject.

#include "object.h"

extern b2World g_b2dPhysicsWorld;

/// Constructor for a game object.
/// \param objecttype Object type.

CGameObject::CGameObject(GameObjectType objecttype){ //constructor
  m_nObjectType = objecttype; 
  m_pBody = NULL;
  m_fXout = 0.0f;
  m_fYout = 0.0f;
  m_nOut = NONE;
} //constructor

/// Destructor for a game object. It takes
/// care of destroying the object's body in Physics World.

CGameObject::~CGameObject(){ //destructor
  if(m_pBody)
    g_b2dPhysicsWorld.DestroyBody(m_pBody);
} //destructor

/// Set the physics body pointer (from Physics World) of the game object.
/// \param b Pointer to the physics body.

void CGameObject::SetPhysicsBody(b2Body* b){
  m_pBody = b;
} //SetPhysicsBody