/// \file renderworld.cpp

#include "renderworld.h"

/// Load game images. Gets file list from gamesettings.xml.

void CRenderWorld::LoadImages(){ //load images
  LoadBackground(); 
  
  //Load sprite for each object
  Load(BALL_OBJECT, "ball"); 
  Load(DOMINO_OBJECT, "domino");
  Load(BUTTON_OBJECT, "button");
  Load(WHEEL_OBJECT, "wheel");
  Load(TRUCK_BODY_OBJECT, "truckBody");
  Load(BASKET_OBJECT, "basketObject");
  Load(BLUE_PORTAL_LEFT_OBJECT, "bluePortalLeft");
  Load(BLUE_PORTAL_RIGHT_OBJECT, "bluePortalRight");
  Load(ORANGE_PORTAL_LEFT_OBJECT, "orangePortalLeft");
  Load(ORANGE_PORTAL_RIGHT_OBJECT, "orangePortalRight");
  Load(ORANGE_PORTAL_UP_OBJECT, "orangePortalUp");
  Load(BLUE_PORTAL_DOWN_OBJECT, "bluePortalDown");
  Load(BLUE_PORTAL_UP_OBJECT, "bluePortalUp");
  Load(COMPANION_CUBE_OBJECT, "companionCube");
  Load(SPACE_CORE_OBJECT, "spaceCore");
  Load(GRAVITY_BUTTON_OBJECT, "gravityButton");
  Load(CAKE_OBJECT, "cake");

  Load(WHITESTAR_OBJECT, "whitestar");
  Load(YELLOWSTAR_OBJECT, "yellowstar");
  Load(REDSTAR_OBJECT, "redstar");
  Load(MAGENTASTAR_OBJECT, "magentastar");
  Load(BLUESTAR_OBJECT, "bluestar");
} //LoadImages