/// \file sndlist.h
/// Enumerated types for sounds.

#pragma once

/// Game sound enumerated type. 
/// These are the sounds used in actual gameplay. Sounds must be listed here in
/// the same order that they are in the sound settings XML file.

enum GameSoundType{ //sounds used in game engine
  THUMP2_SOUND, TICK_SOUND, YAY_SOUND, RADIO_SOUND, PORTAL_SOUND, HEY_SOUND, DIFFERENT_SOUND, BLAME_SOUND, SPACE_SOUND
};