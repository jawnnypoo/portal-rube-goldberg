//Interface for the truck that carries objects in the back

#include "gamedefines.h"
#include "Box2D\Box2D.h"
#include "ObjectManager.h"

class Truck
{
  friend class CObjectWorld;

private:
  b2Body* m_pTruckBody;
  b2Body* m_pWheel1;
  b2Body* m_pWheel2;

  b2WheelJoint* m_pWheelJoint1;
  b2WheelJoint* m_pWheelJoint2;

  b2Body* CreateTruckBody(int x, int y, int nIndex, CGameObject* object); 
  b2Body* CreateWheel(int x, int y, int nIndex, CGameObject* object);

public:
  Truck();
  void Create(float x, float y);
  void MoveIt(float speed);
  void StopIt();
  void FlipIt();
};